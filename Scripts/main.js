﻿$(function() {
    $("#tabSession1").click(function() {
        $("#progressBarSession").css("width", "25%").attr("aria-valuenow", 25);
    });
    $("#tabSession2").click(function () {
        $("#progressBarSession").css("width", "50%").attr("aria-valuenow", 50);
    });
    $("#tabSession3").click(function () {
        $("#progressBarSession").css("width", "75%").attr("aria-valuenow", 75);
    });
    $("#tabSession4").click(function () {
        $("#progressBarSession").css("width", "100%").attr("aria-valuenow", 100);
    });
    $("#datetimepicker1").datetimepicker();
    $(".datetimepicker").datetimepicker();
});